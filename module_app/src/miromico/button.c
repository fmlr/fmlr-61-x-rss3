/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2020 Miromico AG
 * All rights reserved.
 */

#include "button.h"

#include "devboard.h"

#include "common_data.h"

bool getButton() {
    ioport_level_t val;
    ioport_level_t val_prev;
    // simple debouncing
    g_ioport.p_api->pinRead(PIN_BUTTON, &val_prev);
    do {
        tx_thread_sleep(1);
        g_ioport.p_api->pinRead(PIN_BUTTON, &val);
        val_prev = val;
    } while (val != val_prev);
    return val == IOPORT_LEVEL_LOW;
}
