/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2020 Miromico AG
 * All rights reserved.
 */

#include "sht21.h"

#include "lora_thread.h"

#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

#define SHT_READ_TEMP_NOHOLD 	0xF3
#define SHT_READ_TEMP_HOLD   	0xE3
#define SHT_READ_HUM_NOHOLD 	0xF5
#define SHT_READ_HUM_HOLD   	0xE5
#define SHT_READ_USER_REG		0xE7
#define SHT_WRITE_USER_REG		0xE6
#define SHT_SOFT_RESET			0xFE

#define SHT_TEMP_FLAG 			0x00
#define SHT_RH_FLAG				0x02
#define SI7021_READ_HEATER_REG	0x11

#define POLYNOMIAL 0x131 //P(x)=x^8+x^5+x^4+1 = 100110001

static inline bool HW_I2C_Master_Transmit(void * handle, uint8_t * buffer, uint16_t size)
{
    ssp_err_t err;
    err = g_i2c0.p_api->write(g_i2c0.p_ctrl, buffer, size, false);
    return (SSP_SUCCESS == err);
}

static inline bool HW_I2C_Master_Receive(void * handle, uint8_t * buffer, uint16_t size)
{
    ssp_err_t err;
    err = g_i2c0.p_api->read(g_i2c0.p_ctrl, buffer, size, false);
    return (SSP_SUCCESS == err);
}

/**
 * Check CRC

 *
 * @param	data	input data
 * @param	n		number of bytes in input data
 * @param	chck	checksum to match
 * @return	0 on match, -1 on fail
 */
static int8_t SHTCheckCrc(uint8_t data[], uint8_t n, uint8_t chck)
{
  uint8_t crc = 0;
  uint8_t byteCtr;
  //calculates 8-Bit chck with given polynomial
  for (byteCtr = 0; byteCtr < n; ++byteCtr) {
    crc ^= (data[byteCtr]);
    for (uint8_t bit = 8; bit > 0; --bit) {
      if (crc & 0x80) { crc = (crc << 1) ^ POLYNOMIAL; }
      else { crc = (crc << 1); }
    }
  }
  return (crc == chck) ? 0 : -1;
}

/**
 * Read out data from sensor
 *
 * Read out data using the hold commands. 3 bytes are read and
 * checksum is checked
 *
 * @param	i2c		handle of the i2c bus
 * @param	addr	i2c address of the sensor
 * @param	cmd		Temp or humidity read out command
 *
 * @retval	data	data from sensor
 * @return	0 in case of success, -1 otherwise
 */
static int8_t SHTGetValue(SHT_HandleTypedef* handle, uint8_t cmd, uint16_t* data)
{
  uint8_t buf[3];
  buf[0] = cmd;

  assert(data != 0);

  // writes cmd to the I2C
  HW_I2C_Master_Transmit(handle->hi2c, (uint8_t*)&buf, 1);
  tx_thread_sleep (10);

  size_t counter = 0;
  while (1) {
    tx_thread_sleep (5);
    // stop after successful reading
    if (HW_I2C_Master_Receive(handle->hi2c, (uint8_t*)&buf, 3)) {
      break;
    }
    if (counter >= 5) {
      return -1;
    }
    counter++;
  }

  if (SHTCheckCrc(buf, 2, buf[2]) >= 0) {
    *data = (buf[0] << 8 | buf[1]);
    return 0;
  } else {
    return -2;
  }
}

bool SHTInitSensor(SHT_HandleTypedef* handle)
{
  int16_t value = 0;
  bool ok = SHTGetTemp(handle, &value);
  return ok;
}

bool SHTGetTemp(SHT_HandleTypedef* handle, int16_t* temperature)
{
  uint16_t value;
  if (SHTGetValue(handle, SHT_READ_TEMP_NOHOLD, &value) >= 0) {
    if ((value & 0x02) == SHT_TEMP_FLAG) {
      value &= 0xfffc;
      // Formula is T = -46.85 + 175.72 * value / 2^16
      // computing in fixed point
      *temperature = (int16_t)(((-4685 * 16384) + 4393 * (int32_t)value + 8192) / 16384);
      return true;
    }
  }
  // something went wrong
  return false;
}

bool SHTGetHumidity(SHT_HandleTypedef* handle, uint8_t* humidity)
{
  uint16_t value;
  if (SHTGetValue(handle, SHT_READ_HUM_NOHOLD, &value) >= 0) {
    if ((value & 0x02) == SHT_RH_FLAG) {
      value &= 0xfffc;
      // Formula is rH = 6 + 125 * value / 2^16
      // computing in fixed point
      *humidity = (uint8_t)(((-12 * 32768) + 125 * (int32_t)value + 16384) / 32768);
      return true;
    }
  }
  // something wet wrong
  return false;
}
