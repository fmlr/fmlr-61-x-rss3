/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2020 Miromico AG
 * All rights reserved.
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef struct {
  void* hi2c;
} SHT_HandleTypedef;

#define I2C_ADDR_SHT21	0x40

bool SHTInitSensor(SHT_HandleTypedef* handle);
bool SHTGetTemp(SHT_HandleTypedef* handle, int16_t* temperature);
bool SHTGetHumidity(SHT_HandleTypedef* handle, uint8_t* humidity);
