# Renesas SSP driver and example project for Miromico FMLR-61-X-RSS3 LoRaWAN module

     __  __ _____ _____   ____  __  __ _____ _____ ____
    |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
    | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
    | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
    | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
    |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
    Copyright (c) 2020 Miromico AG
    All rights reserved. 

## Introduction

This project enables users of Miromico's FMLR-61-X-RSS3 LoRaWAN module to quickly start developing their own applications. The example code is based on the Renesas Synergy Software Package (SSP). The module and suitable development kits can be purchased on [Avnet Silica's stores](https://www.avnet.com/wps/portal/silica/products/new-products/npi/2019/renesas-fmlr61xrss3-lora/).

* The software is compliant with [LoRaWAN Specification v1.0.3](https://lora-alliance.org/resource-hub/lorawantm-specification-v103). Class A, Class B and Class C end-device classes are supported. Further information about the LoRaWAN Specification can be found at: https://lora-alliance.org/lorawan-for-developers

## Supported platforms

This project provides support for the platforms below.

* Miromico FMLR-61-X-RSS3 LoRaWAN Module
  * Renesas Synergy Platform SSP V1.7.0
  * S3A678CNE Controller

## Usage

Prior to building the project, the .pack file contained in the repository need to be copied into your e2 studio installation's .pack folder. Then open the application in Renesas e2 studio. You should now be able to build and debug the application just like any other SSP project. Note that the LoRaWAN Stack is running in a thread independent to your application thread.

## Documentation

A quick start guide and further information can be found on [Miromico's Website](https://miromico.ch/). Scroll down to the download section of the particular product pages.
 * [FMLR-61-X-RSS3 product page](https://miromico.ch/portfolio/fmlr_renesas/)
 * [FMLR development kit product page](https://miromico.ch/portfolio/fmlr-development-board/)

## CI / Status

The software project currently has one active branche in place. The **[master](https://gitlab.com/fmlr/fmlr-61-x-rss3)** branch provides the latest released source code.

[`master`](https://gitlab.com/fmlr/fmlr-61-x-rss3) | [![Build Status](https://clavion.miromico.ch/renesas/fmlr-61-x-rss3_lorawan_ssp_module//badges/release_v1/pipeline.svg)](https://gitlab.com/fmlr/fmlr-61-x-rss3)

## Authors
  * Rolf Meeser, Renesas Electronics Corporation
  * Andreas Senn, Miromico AG

## Acknowledgments

This project is based on Semtech's official BasicMAC (https://github.com/lorabasics/basicmac) V2.1 LoRaWAN stack. For further information regarding the stack's internals, please consult the [BasicMAC documentation](https://doc.sm.tc/mac/)

## Changelog

### 2020-04-06, V1.1

* General
    * SSP module .pack files for LoRaWAN driver
    * Example project for development board